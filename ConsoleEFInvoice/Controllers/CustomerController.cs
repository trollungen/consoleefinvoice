﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Diagnostics;
using ConsoleEFInvoice.Bill;
using System.Diagnostics;

namespace ConsoleEFInvoice.Controllers
{
    public class CustomerController
    {
        public static void MakeCustomer()
        {
            Console.Write("Förnamn: ");
            var firstName = Console.ReadLine();
            Console.Write("Efternamn: ");
            var lastName = Console.ReadLine();
            Console.Write("Adress: ");
            var address = Console.ReadLine();
            Console.Write("Ort: ");
            var city = Console.ReadLine();

            var newcustomer = new Customer { FirstName = firstName, LastName = lastName, Address = address, City = city };

            var proxy = new DbProxy();
            proxy.AddCustomer(newcustomer);
        }

        public static void MakeOrder()
        {
            var proxy = new DbProxy();
            Console.WriteLine("Lägga en order");
            Console.WriteLine();
            var listofproducts = proxy.GetListOfAllProduct();

            Console.WriteLine(
                "Articel ID\tProductName\tPrice"
                );

            foreach (var item in listofproducts)
            {
                Console.WriteLine(
                    item.ArticelId + "\t" +
                    item.ProductName + "\t" +
                    item.Price);

            }
            Console.WriteLine("Vilken produkt vill du köpa?");

            Console.Write("Ange ArtikelId: ");
            var id = int.Parse(Console.ReadLine());
            Console.Write("Hur många av varje?: ");
            var amount = int.Parse(Console.ReadLine());
            Console.WriteLine("Ditt Kundnummer: ");
            var kund = int.Parse(Console.ReadLine());


            var product = proxy.GetListOfAllProduct().FirstOrDefault(x => x.ArticelId == id);
            var orders = proxy.GetListOfAllOrders().OrderByDescending(u => u.OrderNr).FirstOrDefault();

            var newOrder = new OrderProduct
            {
                CustomerId = kund,
                ProductName = product.ProductName,
                Qty = amount,
                Status = 0,
                UnitPrice = product.Price,
                OrderNr = orders.OrderNr++,
            };

            proxy.AddOrderProduct(newOrder);

        }

    }
}
