﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Diagnostics;
using ConsoleEFInvoice.Bill;


namespace ConsoleEFInvoice.Controllers
{
    public class ProductController
    {
        public static void UppdatePriceInDB()
        {
            var proxy = new DbProxy();
            Console.WriteLine("Med hur mycket vill du ändra priserna på alla produkter?");
            Console.WriteLine("Skriv så mycket som det nuvarande priset bör multipliceras med (använd komma)");
            Console.Write("Input: ");

            var stopWatch = new Stopwatch();
            try
            {
                decimal value = decimal.Parse(Console.ReadLine());
                stopWatch.Start();
                proxy.UpdateAllPrice(value);
                stopWatch.Stop();
                Console.WriteLine("Tid: " + stopWatch.ElapsedMilliseconds + "ms");
            }
            catch (Exception)
            {
                Console.WriteLine("Du måste skriva med Komma");
            }

        }

        public static void UpdatePriceWithTransaction()
        {
            var proxy = new DbProxy();
            var productList = proxy.GetListOfAllProduct();

            foreach (var item in productList)
            {
                Console.WriteLine(item.Id + " " + item.Price + " " + item.ProductName);
            }

            Console.WriteLine();
            Console.Write("Vilken produkt vill du öka priset på?: ");
            var increase = int.Parse(Console.ReadLine());

            Console.Write("Vilken produkt vill du sänka priset på?: ");
            var decrease = int.Parse(Console.ReadLine());

            Console.Write("Hur mycket vill du ändra priset? (kr): ");
            var amount = decimal.Parse(Console.ReadLine());

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            using (var scope = new TransactionScope())
            {
                try
                {
                    proxy.IncreasePrice(increase, amount);
                    proxy.DecreasePrice(decrease, amount);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Tid: " + stopWatch.ElapsedMilliseconds + "ms");
            Console.ForegroundColor = ConsoleColor.Gray;

        }

        public static void UpdatePriceWithoutTransaction()
        {
            var proxy = new DbProxy();
            Console.WriteLine();
            var increase = 1;
            var decrease = 2;
            var amount = 5;

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            try
            {
                proxy.IncreasePrice(increase, amount);
                proxy.DecreasePrice(decrease, amount);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Tid: " + stopWatch.ElapsedMilliseconds + "ms");
            Console.ForegroundColor = ConsoleColor.Gray;

        }

    }
}
