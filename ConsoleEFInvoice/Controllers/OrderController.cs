﻿using ConsoleEFInvoice.Bill;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleEFInvoice.Bill;

namespace ConsoleEFInvoice.Controllers
{
    class OrderController
    {
        public static void GetALLOrders()
        {
            var proxy = new DbProxy();
            var stopWatch1 = new Stopwatch();
            var stopWatch2 = new Stopwatch();
            stopWatch1.Start();
            var orders = proxy.GetListOfAllOrders();
            stopWatch1.Stop();

            stopWatch2.Start();
            foreach (var item in orders)
            {
                Console.WriteLine(
                    item.Status + "\t" +
                    item.OrderNr + "\t" +
                    item.ProductName + "\t" +
                    item.Qty + "\t" +
                    item.UnitPrice
                    );
            }
            stopWatch2.Stop();

            Console.WriteLine("Tog " + stopWatch1.ElapsedMilliseconds + " att hämta listan");
            Console.WriteLine("Tog " + stopWatch2.ElapsedMilliseconds + " att skriva ut listan");

        }

        public static void CreateInvoice()
        {
            var proxy = new DbProxy();

            var currentInvoiceNr = 1;
            try
            {
                currentInvoiceNr = proxy.GetListOfAllInvoice().Max(x => x.InvoiceNumber);
            }
            catch (Exception)
            {
                Console.WriteLine("Inga fakturor är skapade");
            }
            Console.WriteLine("Senaste fakturanumret: " + (currentInvoiceNr - 1));

            var listOfOrders = proxy.GetListOfAllOrders().GroupBy(x => x.OrderNr).Select(q => q.First()).Where(r => r.Status == 0);

            Console.WriteLine("Antal ej fakturerade ordrar: " + listOfOrders.Count());
            Console.WriteLine();

            if (listOfOrders.Count() != 0)
            {
                Console.Write("Skapa ordrar tryck enter");
                Console.ReadLine();

                var listofOrder = proxy.GetListOfAllOrders();
                var stopWatch = new Stopwatch();
                try
                {
                    bool status = true;
                    stopWatch.Start();
                    foreach (var item in listOfOrders)
                    {
                        var customer = proxy.GetListOfAllCustomer().FirstOrDefault(u => u.Id == item.CustomerId);
                        var newInvoice = new Invoice
                        {
                            FullName = customer.FirstName + " " + customer.LastName,
                            Address = customer.Address,
                            City = customer.City,
                            OrderNr = item.OrderNr,
                            Created = DateTime.Now,
                            InvoiceNr = ++currentInvoiceNr,
                        };

                        proxy.CreateInvoice(newInvoice);
                    }

                    stopWatch.Stop();
                    proxy.UpdateOrderStatus(status);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Skapade fakturor på: " + stopWatch.ElapsedMilliseconds + " ms");
                    Console.ForegroundColor = ConsoleColor.Gray;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Det finns inga fakturor att skapa");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

        }
    }
}
