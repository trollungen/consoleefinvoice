﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEFInvoice.Bill
{
    public class InvoiceModel
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public int InvoiceNumber { get; set; }
        public int OrderNumber { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }
}
