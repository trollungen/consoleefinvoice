﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Diagnostics;

namespace ConsoleEFInvoice.Bill
{
    public class DbProxy
    {
        private BillDbEntities context;

        public DbProxy()
        {
            context = new BillDbEntities();
        }
        public BillDbEntities GetEntityConnection()
        {
            return context;
        }

        public void AddCustomer(Customer customer)
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                context.Customers.Add(customer);
                context.SaveChanges();
                stopWatch.Stop();
                Console.WriteLine("Saving Customer in: " + stopWatch.ElapsedMilliseconds + " ms");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddOrderProduct(OrderProduct order)
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                context.OrderProducts.Add(order);
                context.SaveChanges();
                stopWatch.Stop();
                Console.WriteLine("Saving Product in: " + stopWatch.ElapsedMilliseconds + " ms");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public IEnumerable<ProductModel> GetListOfAllProduct()
        {
            return context.Products.Select(x => new ProductModel
            {
                Id = x.Id,
                ArticelId = x.ArticleId,
                Description = x.Description,
                Price = x.Price,
                ProductName = x.ProductName,

            }).ToList();

        }

        public IEnumerable<CustomerModel> GetListOfAllCustomer()
        {
            return context.Customers.Select(x => new CustomerModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Address = x.Address,
                City = x.City,

            }).ToList();

        }

        public IEnumerable<OrderProductModel> GetListOfAllOrders()
        {
            return context.OrderProducts.Select(x => new OrderProductModel
            {
                Id = x.Id,
                Status = x.Status,
                CustomerId = x.CustomerId,
                OrderNr = x.OrderNr,
                ProductName = x.ProductName,
                Qty = x.Qty,
                UnitPrice = x.UnitPrice,

            }).ToList();

        }

        public IEnumerable<InvoiceModel> GetListOfAllInvoice()
        {
            return context.Invoices.Select(x => new InvoiceModel
            {
                Id = x.Id,
                Created = x.Created,
                InvoiceNumber = x.InvoiceNr,
                OrderNumber = x.OrderNr,
                FullName = x.FullName,
                Address = x.Address,
                City = x.City

            }).ToList();

        }

        public void UpdateAllPrice(decimal number)
        {
            var listofproduct = from list in context.Products
                                where list.Id > 0
                                select list;

            foreach (var item in listofproduct)
            {
                item.Price = item.Price * number;
            }
            context.SaveChanges();

        }

        public void CreateInvoice(Invoice invoice)
        {

            context.Invoices.Add(invoice);
            context.SaveChanges();

        }


        internal void UpdateOrderStatus(bool status)
        {
            if (status)
            {
                var listoforders = from list in context.OrderProducts
                                   where list.Status == 0
                                   select list;
                foreach (var item in listoforders)
                {
                    item.Status = 1;
                }
                context.SaveChanges();
            }
        }

        internal void IncreasePrice(int productId, decimal price)
        {

            context.Products.FirstOrDefault(x => x.Id == productId).Price = context.Products.FirstOrDefault(x => x.Id == productId).Price + price;
            context.SaveChanges();
        }

        internal void DecreasePrice(int productId, decimal price)
        {

            if (context.Products.FirstOrDefault(x => x.Id == productId).Price - price < 0)
            {
                throw new ArgumentException("Negativ summa");
            }

            context.Products.FirstOrDefault(x => x.Id == productId).Price = context.Products.FirstOrDefault(x => x.Id == productId).Price - price;
            context.SaveChanges();

        }
    }
}
