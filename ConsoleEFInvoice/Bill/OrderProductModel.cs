﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEFInvoice.Bill
{
    public class OrderProductModel
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public int CustomerId { get; set; }
        public int OrderNr { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
