﻿using ConsoleEFInvoice.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleEFInvoice
{
    class EFOperationApp
    {
        public EFOperationApp()
        {
            Console.WindowHeight = 30;
            Console.WindowWidth = 150;
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Välkommen till Console EF App");
            Console.ForegroundColor = ConsoleColor.Gray;

            bool loop = true;
            while (loop)
            {
                PrintMenuOptions();
                loop = HandleMenuOptions(loop);
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Välkommen tillbaka");
            Thread.Sleep(500);
        }

        public bool HandleMenuOptions(bool loop)
        {
            var context = new BillDbEntities();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Input: ");
            string input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Gray;

            if (input.Equals("1")) { CustomerController.MakeCustomer(); }
            if (input.Equals("2")) { CustomerController.MakeOrder(); }
            if (input.Equals("3")) { OrderController.GetALLOrders(); }
            if (input.Equals("4")) { ProductController.UppdatePriceInDB(); }
            if (input.Equals("5")) { OrderController.CreateInvoice(); }
            if (input.Equals("6")) { ProductController.UpdatePriceWithTransaction(); }
            if (input.Equals("7")) { ProductController.UpdatePriceWithoutTransaction(); }
            if (input.Equals("0")) { loop = false; }

            return loop;
        }

        private void PrintMenuOptions()
        {
            Console.WriteLine();
            Console.WriteLine("1. Skapa En Customer");
            Console.WriteLine("2. Skapa en Order/ett Köp");
            Console.WriteLine("3. Se alla Ordrar");
            Console.WriteLine("4. Uppdatera alla priser i databasen");
            Console.WriteLine("5. Skapa fakturor");
            Console.WriteLine("6. Update Price with Transaction");
            Console.WriteLine("7. Update Price without Transaction");
            Console.WriteLine();
            Console.WriteLine("0. EXIT");
            Console.WriteLine();
        }

    }
}

